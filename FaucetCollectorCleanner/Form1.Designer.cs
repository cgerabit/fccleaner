﻿namespace FaucetCollectorCleanner
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonStart = new System.Windows.Forms.Button();
            this.CheckBoxMemory = new System.Windows.Forms.CheckBox();
            this.CheckBoxMinutes = new System.Windows.Forms.CheckBox();
            this.NumericMinutes = new System.Windows.Forms.NumericUpDown();
            this.NumericMemory = new System.Windows.Forms.NumericUpDown();
            this.IncludeChrome = new System.Windows.Forms.CheckBox();
            this.ProcessName = new System.Windows.Forms.TextBox();
            this.LabelName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NumericMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericMemory)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonStart
            // 
            this.ButtonStart.Location = new System.Drawing.Point(466, 90);
            this.ButtonStart.Name = "ButtonStart";
            this.ButtonStart.Size = new System.Drawing.Size(75, 23);
            this.ButtonStart.TabIndex = 0;
            this.ButtonStart.Text = "Start";
            this.ButtonStart.UseVisualStyleBackColor = true;
            this.ButtonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // CheckBoxMemory
            // 
            this.CheckBoxMemory.AutoSize = true;
            this.CheckBoxMemory.Location = new System.Drawing.Point(12, 44);
            this.CheckBoxMemory.Name = "CheckBoxMemory";
            this.CheckBoxMemory.Size = new System.Drawing.Size(165, 17);
            this.CheckBoxMemory.TabIndex = 2;
            this.CheckBoxMemory.Text = "Clean If the memory wasted >";
            this.CheckBoxMemory.UseVisualStyleBackColor = true;
            this.CheckBoxMemory.CheckedChanged += new System.EventHandler(this.CheckBoxMemory_CheckedChanged);
            // 
            // CheckBoxMinutes
            // 
            this.CheckBoxMinutes.AutoSize = true;
            this.CheckBoxMinutes.Location = new System.Drawing.Point(12, 110);
            this.CheckBoxMinutes.Name = "CheckBoxMinutes";
            this.CheckBoxMinutes.Size = new System.Drawing.Size(129, 17);
            this.CheckBoxMinutes.TabIndex = 3;
            this.CheckBoxMinutes.Text = "Clean Every(Minutes):";
            this.CheckBoxMinutes.UseVisualStyleBackColor = true;
            this.CheckBoxMinutes.CheckedChanged += new System.EventHandler(this.CheckBoxMinutes_CheckedChanged);
            // 
            // NumericMinutes
            // 
            this.NumericMinutes.Location = new System.Drawing.Point(171, 107);
            this.NumericMinutes.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.NumericMinutes.Name = "NumericMinutes";
            this.NumericMinutes.Size = new System.Drawing.Size(120, 20);
            this.NumericMinutes.TabIndex = 4;
            // 
            // NumericMemory
            // 
            this.NumericMemory.Location = new System.Drawing.Point(183, 44);
            this.NumericMemory.Name = "NumericMemory";
            this.NumericMemory.Size = new System.Drawing.Size(120, 20);
            this.NumericMemory.TabIndex = 5;
            this.NumericMemory.ValueChanged += new System.EventHandler(this.NumericUpDown1_ValueChanged);
            // 
            // IncludeChrome
            // 
            this.IncludeChrome.AutoSize = true;
            this.IncludeChrome.Enabled = false;
            this.IncludeChrome.Location = new System.Drawing.Point(12, 67);
            this.IncludeChrome.Name = "IncludeChrome";
            this.IncludeChrome.Size = new System.Drawing.Size(167, 17);
            this.IncludeChrome.TabIndex = 6;
            this.IncludeChrome.Text = "Include Chrome-ChromeDriver";
            this.IncludeChrome.UseVisualStyleBackColor = true;
            // 
            // ProcessName
            // 
            this.ProcessName.Location = new System.Drawing.Point(343, 162);
            this.ProcessName.Name = "ProcessName";
            this.ProcessName.Size = new System.Drawing.Size(142, 20);
            this.ProcessName.TabIndex = 7;
            // 
            // LabelName
            // 
            this.LabelName.AutoSize = true;
            this.LabelName.Location = new System.Drawing.Point(363, 146);
            this.LabelName.Name = "LabelName";
            this.LabelName.Size = new System.Drawing.Size(92, 13);
            this.LabelName.TabIndex = 8;
            this.LabelName.Text = "FC Process Name";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 203);
            this.Controls.Add(this.LabelName);
            this.Controls.Add(this.ProcessName);
            this.Controls.Add(this.IncludeChrome);
            this.Controls.Add(this.NumericMemory);
            this.Controls.Add(this.NumericMinutes);
            this.Controls.Add(this.CheckBoxMinutes);
            this.Controls.Add(this.CheckBoxMemory);
            this.Controls.Add(this.ButtonStart);
            this.Name = "Form1";
            this.Text = "FaucetCollectorCleaner";
            ((System.ComponentModel.ISupportInitialize)(this.NumericMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericMemory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonStart;
        private System.Windows.Forms.CheckBox CheckBoxMemory;
        private System.Windows.Forms.CheckBox CheckBoxMinutes;
        private System.Windows.Forms.NumericUpDown NumericMinutes;
        private System.Windows.Forms.NumericUpDown NumericMemory;
        private System.Windows.Forms.CheckBox IncludeChrome;
        private System.Windows.Forms.TextBox ProcessName;
        private System.Windows.Forms.Label LabelName;
    }
}

