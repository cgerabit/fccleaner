﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaucetCollectorCleanner
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
            NumericMemory.Maximum = 500000;
            NumericMemory.Value = 500;
            NumericMinutes.Maximum = 500000;
            NumericMinutes.Value = 180;
            ExecutionCheckMemory = false;
            ExecutionCheckMinutes = false;
            ProcessName.Text = "FaucetCollector.exe";
            Started = false;
        }
        private bool Started { get; set; }
        private DateTime StartedTime { get; set; }
        private bool ExecutionCheckMemory { get; set; }
        private bool ExecutionCheckMinutes { get; set; }
        private void ButtonStart_Click(object sender, EventArgs e)
        {
           
            if (!Started)
            {
                if (!CheckBoxMemory.Checked && !CheckBoxMinutes.Checked)
                {
                    MessageBox.Show("Need to select at least one of the options");
                }
                else if (!File.Exists(Path.Combine(Directory.GetCurrentDirectory(), ProcessName.Text)))
                {
                    MessageBox.Show("FaucetCollector executable not found");
                }
                else
                {
                    ButtonStart.Text = "Stop";
                    Started = true;
                    CheckMemory();
                    int Minutes = Convert.ToInt32(NumericMinutes.Value);
                    CheckMinutes(Minutes);
                }
            }
            else
            {
                Started = false;
                ButtonStart.Text = "Start";
            }
        }

        public async void CheckMemory()
        {
            ExecutionCheckMemory = true;
            while (Started && CheckBoxMemory.Checked)
            {
                Process[] DesktopProcess = Process.GetProcesses();
                long Memory = 0;
                try
                {
                    if (IncludeChrome.Checked)
                    {
                        foreach (Process Process in DesktopProcess.Where(Process => Process.ProcessName == ProcessName.Text.Split('.')[0] || Process.ProcessName == "chrome" || Process.ProcessName.Contains("chromedriver")).Select(Process => Process))
                        {
                            Memory += Process.VirtualMemorySize64;
                        }
                    }
                    else
                    {
                        foreach (Process Process in DesktopProcess.Where(Process => Process.ProcessName == ProcessName.Text.Split('.')[0]).Select(Process => Process))
                        {
                            Memory += Process.VirtualMemorySize64;
                            break;
                        }
                    }
                }
                catch (Exception)
                {

                }
                if ((Memory / (1024 * 1024 * 8)) > (long)NumericMemory.Value)
                {

                    Restart();

                }


                await Task.Delay(300000);
            }
            ExecutionCheckMemory = false;
        }
        public async void CheckMinutes(int Minutes)
        {
            ExecutionCheckMinutes = true;
            StartedTime = DateTime.Now.AddMinutes(Minutes);
            while (Started && CheckBoxMinutes.Checked)
            {
                if (DateTime.Now > StartedTime)
                {
                    Restart();


                }

                await Task.Delay(300000);
            }
            ExecutionCheckMinutes = false;

        }
        public void Restart()
        {
            Process[] DesktopProcess = Process.GetProcesses();
            foreach (Process Process in from Process Process in DesktopProcess
                                        where Process.ProcessName == ProcessName.Text.Split('.')[0] || Process.ProcessName == "chrome" || Process.ProcessName == "chromedriver"
                                        select Process)
            {
                try
                {
                    Process.Kill();
                }
                catch (Exception)
                {

                }
            }

            ProcessStartInfo info = new ProcessStartInfo
            {
                UseShellExecute = true,
                FileName = "FaucetCollector.exe",
                WorkingDirectory = Directory.GetCurrentDirectory(),
                Arguments = "/autostart"
            };

            Process.Start(info);
            try
            {
                StartedTime = DateTime.Now.AddMinutes(Convert.ToInt32(NumericMinutes.Value));
            }
            catch (Exception)
            {
                StartedTime = DateTime.Now.AddMinutes(120);
            }

        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void CheckBoxMemory_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxMemory.Checked)
            {
                if (!ExecutionCheckMemory && Started)
                {
                    CheckMemory();
                }
                IncludeChrome.Enabled = true;
            }
            else
            {
                IncludeChrome.Enabled = false;
                IncludeChrome.Checked = false;
            }
        }

        private void CheckBoxMinutes_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxMemory.Checked)
            {
                if (!ExecutionCheckMinutes && Started)
                {
                    CheckMinutes(Convert.ToInt32(NumericMinutes.Value));
                }
            }
        }
    }
}
